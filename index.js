var cool = require('cool-ascii-faces');
var express = require('express');
var app = express();
var pg = require('pg');


app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index')
});

app.get('/cool', function(request, response) {
  response.send(cool());
});
// app.get('/db', function (request, response) {
//   // pg.connect(process.env.DATABASE_URL, function(err, client, done) {
//   //   client.query('SELECT * FROM test_table', function(err, result) {
//   //     done();
//   //     if (err)
//   //      { console.error(err); response.send("Error " + err); }
//   //     else
//   //      { response.render('pages/db', {results: result.rows} ); }
//   //   });
//   // });
//   // create a pool
// var pool = new pg.Pool()

// // // connection using created pool
// var connectionString = "postgres://astwctzedkjgny:2e8de1a5893d312e4a5ae9e6dc5f39ab40691c1a1ec1727540ac5279f97f75ec@ec2-50-16-202-213.compute-1.amazonaws.com:5432/d8ltg1d9dq25um"

// pool.connect(connectionString,function(err, client, done) {
//   console.log(process.env.DATABASE_URL);
//     client.query('SELECT * FROM test_table', function(err, result) {
//       done();
//       if (err)
//        { console.error(err); response.send("Error " + err); }
//       else
//        { response.render('pages/db', {results: result.rows} ); }
//     });

// // const query = client.query('SELECT * FROM test_table')
// // assert(query instanceof Promise) // true
// // assert(query.on === undefined) // true
// // query.then((res) =>  response.render('pages/db', {results: result.rows} ));
// });
// // // pool shutdown
// // pool.end()
// });

app.get('/db', function (request, response) {
  pg.connect(process.env.DATABASE_URL, function(err, client, done) {
    client.query('SELECT * FROM test_table', function(err, result) {
      done();
      if (err)
       { console.error(err); response.send("Error " + err); }
      else
       { response.render('pages/db', {results: result.rows} ); }
    });
  });
});
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});